# Welcome to [accessibility allies](https://www.a11y-allies.com)

This little side project aims to list and promote web accessibility through articles, blogs and other resources from around the internet.

## 🚀 Project Structure

### Astro global structure

The website is built with [Astro](https://astro.build). Inside the project, you'll see the following folders and files (non exhaustive):

```
/
├── public/
│   ├── _headers
│   ├── assets
│   │   ├── fonts
│   │   └── images
│   ├── robots.txt
│   └── favicon.ico
├── src/
│   ├── components/
│   │   └── Header.astro
│   ├── content/
│   │   ├── people
│   │   │   └── some-file.md
│   │   └── websites
│   │       └── some-file.md
│   ├── layouts/
│   │   └── default.astro
│   ├── pages/
│   │   ├── [type]
│   │   │   └── [slug].astro
│   │   ├── about.md
│   │   └── index.astro
│   └── styles/
│       ├── global.css
│       └── post.css
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

### Specific structure to this project

The content or "posts" that are displayed on the website come from `.md` files placed in `src > content` rather than `src > pages`.

We then fetch them using `Astro.fetchContent()` from the pages.

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command        | Action                                       |
| :------------- | :------------------------------------------- |
| `yarn install` | Installs dependencies                        |
| `yarn dev`     | Starts local dev server at `localhost:3000`  |
| `yarn build`   | Build your production site to `./dist/`      |
| `yarn preview` | Preview your build locally, before deploying |

## 👀 Want to learn more?

Feel free to check [our documentation](https://github.com/withastro/astro) or jump into our [Discord server](https://astro.build/chat).
