---
type: websites
lang: ['english']
title: 'A11Y collective'
link: 'https://www.a11y-collective.com/'
---

The A11Y Collective is a course platform that specialize in accessibility learning.

They publish [articles](https://www.a11y-collective.com/blog/), one of my favourite being [“Blind people don’t visit my website”](https://www.a11y-collective.com/blog/blind-people-dont-visit-my-website/).

[Visit their website!](https://www.a11y-collective.com/)
