---
type: 'websites'
lang: ['french']
title: 'Access 42'
link: 'https://access42.net/'
---

Access42 is a french accessibility agency.

They perform accessibility audits and advise clients on accessibility decisions.

They publish [articles](https://access42.net/blog) and lists of useful accessibility resources.

[Visit their website!](https://access42.net/)
