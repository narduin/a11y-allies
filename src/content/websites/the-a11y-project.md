---
type: websites
lang: ['english']
title: 'The A11Y Project'
link: 'https://www.a11yproject.com/'
---

“The A11Y Project is a community-driven effort to make digital accessibility easier.”

They publish articles, checklists and curated lists of useful accessibility resources.

[Visit their website!](https://www.a11yproject.com)
