---
type: people
lang: [english]
title: Heydon pickering
link: https://heydonworks.com/
---

Heydon is a designer and developer.

He writes [books](https://www.smashingmagazine.com/printed-books/inclusive-components/) and articles about front-end development, usually focusing on inclusivity and accessibility. He even makes [videos](https://briefs.video/) about front-end too.

He also [talks](https://www.youtube.com/watch?v=ga_byUbqvCc) at cool conferences.

[Visit his website!](https://heydonworks.com/)
