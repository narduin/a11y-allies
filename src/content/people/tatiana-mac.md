---
type: 'people'
lang: [english]
title: Tatiana Mac
link: https://www.tatianamac.com/
---

Tatiana is “an independent American engineer”.

They write [articles](https://www.tatianamac.com/writing) about front-end development as well as more general “human” topics.

They also [talk](https://www.youtube.com/watch?v=KxT0EwGrXWU) about “accessibility, design systems, performance, and inclusion”.

[Visit their website!](https://www.tatianamac.com/)
