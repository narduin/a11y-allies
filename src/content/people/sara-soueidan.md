---
type: 'people'
lang: [english]
title: 'Sara Soueidan'
link: 'https://www.sarasoueidan.com'
---

Sara is an “independent front-end UI/design engineer, author and speaker”.

She writes [articles](https://www.sarasoueidan.com/blog/) about design and front-end development, focusing mainly on accessibility.

She also [talks](https://www.youtube.com/watch?v=Mv_RlmAm4nc), among other things, about accessibility, design and inclusivity.

[Visit her website!](https://www.sarasoueidan.com/)
