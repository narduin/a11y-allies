---
layout: '../layouts/PostLayout.astro'
title: 'About'
---

# About this website

## Why this website?

I've been gathering resources about accessibility for a while now. Not being very organized with my browser's bookmarks, I often can't find most of the articles and websites I put aside.

I read Netlify's [Dusty Domains announcement](https://www.netlify.com/blog/2021/12/01/dusty-domains-your-forgotten-domains-raise-money-for-charity/) and thought it could be the perfect reason/motivation I needed to finally organize clearly my accessibility resources. Making it available publicly is obviously a bonus!

## Who?

I'm an independant front-end designer/developer trying to make accessible products and spread awareness about it.

## Want to add/remove something here?

Sure thing!

### Add something

The content is written in markdown so you can open a pull request on [the project's gitlab repo](https://gitlab.com/narduin/a11y-allies).

If you think something could be improved on accessibility allies, don't hesitate to open an issue or a pull request as well!

### Remove something

You found yourself listed on this website and you don't want to be? Send me an email [here](mailto:contact@a11y-allies.com) and I'll remove you.

## Credits

I built this website using [astro](https://astro.build/) as I was very curious about the project.

I was able to make it work thanks to multiple examples and public projects I found on the project's issues. So a big shout out to [Caleb Jasik](https://github.com/jasikpark), [Nauris Pūķis](https://github.com/pyronaur) and, of course, the astro [team](https://github.com/withastro/astro/graphs/contributors)!

Huge thanks to [Lynn Fisher](https://lynnandtonic.com/) for her design advice.

## Privacy

This website is hosted by [Netlify](https://netlify.com/) Inc., 2325 3rd Street, Suite 296, San Francisco, California 94107.

This website does not use cookies, tracking or anything of that sort. I mean, we mint NFTs with your CPU but that's it ([just kidding](https://everestpipkin.medium.com/but-the-environmental-issues-with-cryptoart-1128ef72e6a3), [of course](https://thatkimparker.medium.com/most-artists-are-not-making-money-off-nfts-and-here-are-some-graphs-to-prove-it-c65718d4a1b8)).
